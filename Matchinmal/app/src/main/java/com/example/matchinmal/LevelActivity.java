package com.example.matchinmal;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class LevelActivity extends AppCompatActivity {

    private Button easy, medium, hard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level);

        easy = (Button)findViewById(R.id.easy);
        easy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OpenEasyMainActivity();
            }
        });

        medium = (Button)findViewById(R.id.medium);
        medium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OpenMediumMainActivity();
            }
        });

        hard = (Button)findViewById(R.id.hard);
        hard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OpenHardMainActivity();
            }
        });
    }

    public void OpenEasyMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("intVariableName", 1);
        startActivity(intent);
    }

    public void OpenMediumMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("intVariableName", 2);
        startActivity(intent);
    }

    public void OpenHardMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("intVariableName", 3);
        startActivity(intent);
    }
}
