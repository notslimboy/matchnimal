package com.example.matchinmal;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainMenuActivity extends AppCompatActivity {
    private Button button, quitButton, credit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        button = (Button) findViewById(R.id.PlayButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OpenMainActivity();
            }
        });

        credit = (Button) findViewById(R.id.CreditButton);
        credit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OpenCreditActivity();
            }
        });

        quitButton = (Button)findViewById(R.id.QuitButton);
        quitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.exit(0);
            }
        });
    }

    public void OpenMainActivity() {
        Intent intent = new Intent(this, LevelActivity.class);
        startActivity(intent);
    }

    public void OpenCreditActivity() {
        Intent intent = new Intent(this, CreditActivity.class);
        startActivity(intent);
    }
}
