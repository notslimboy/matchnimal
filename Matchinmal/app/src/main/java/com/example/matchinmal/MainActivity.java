package com.example.matchinmal;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Arrays;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    ImageView curView = null;
    TextView countDownText;
    CountDownTimer countDownTimer;
    long timerValue;
    int minute;
    public boolean over = false;

    private InterstitialAd mInterstitialAd;

    private int countPair = 0;
    final int[] drawable = new int[] {
            R.drawable.sample_0,
            R.drawable.sample_1,
            R.drawable.sample_2,
            R.drawable.sample_3,
            R.drawable.sample_4,
            R.drawable.sample_5,
            R.drawable.sample_6,
            R.drawable.sample_7
    };

    int[] posA = new int[8];
    int[] posB = new int[8];

    int[] pos = {0,1,2,3,4,5,6,7,0,1,2,3,4,5,6,7};

    int currentPos = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent mIntent = getIntent();
        int intValue = mIntent.getIntExtra("intVariableName", 0);

        if(intValue == 1){
            timerValue = 90000;
        }
        else if(intValue == 2){
            timerValue = 60000;
        }
        else if(intValue == 3){
            timerValue = 30000;
        }

        countDownText = findViewById(R.id.CountDownText);
        countDownText.setTextColor(Color.WHITE);
        countDownTimer = new CountDownTimer(timerValue, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                //Toast.makeText(MainActivity.this, "time start", Toast.LENGTH_SHORT).show();

                if(millisUntilFinished / 1000 > 120){
                    minute = 2;
                    millisUntilFinished = millisUntilFinished - 60000;
                }
                else if(millisUntilFinished / 1000 > 60){
                    minute = 1;
                    millisUntilFinished = millisUntilFinished - 60000;
                }
                else{
                    minute = 0;
                }

                if(minute == 0){
                    if(millisUntilFinished / 1000 >= 10){
                        countDownText.setText("00:" + millisUntilFinished / 1000 + " left");
                    }
                    else if(millisUntilFinished / 1000 < 10){
                        countDownText.setText("00:0" + millisUntilFinished / 1000 + " left");
                    }
                }
                else if(minute == 1){
                    if(millisUntilFinished / 1000 >= 10){
                        countDownText.setText("01:" + millisUntilFinished / 1000 + " left");
                    }
                    else if(millisUntilFinished / 1000 < 10){
                        countDownText.setText("01:0" + millisUntilFinished / 1000 + " left");
                    }
                }
                else if(minute == 2){
                    if(millisUntilFinished / 1000 >= 10){
                        countDownText.setText("02:" + millisUntilFinished / 1000 + " left");
                    }
                    else if(millisUntilFinished / 1000 < 10){
                        countDownText.setText("02:0" + millisUntilFinished / 1000 + " left");
                    }
                }
            }

            @Override
            public void onFinish() {
                countDownText.setText("time stop. game over");
                //Toast.makeText(MainActivity.this, "finish", Toast.LENGTH_SHORT).show();
                over = true;
                finish();

                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                } else {
                    Toast.makeText(MainActivity.this, "Interstitial is not loaded.", Toast.LENGTH_LONG).show();
                }
            }
        };

        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        first();
        doBindService();
        Intent music = new Intent();
        music.setClass(this, MusicService.class);
        startService(music);
    }

    //Bind/Unbind music service
    private boolean mIsBound = false;
    private MusicService mServ;
    private ServiceConnection Scon =new ServiceConnection(){

        public void onServiceConnected(ComponentName name, IBinder
                binder) {
            mServ = ((MusicService.ServiceBinder)binder).getService();
        }

        public void onServiceDisconnected(ComponentName name) {
            mServ = null;
        }
    };

    void doBindService(){
        bindService(new Intent(this,MusicService.class),
                Scon, Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }

    void doUnbindService()
    {
        if(mIsBound)
        {
            unbindService(Scon);
            mIsBound = false;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mServ != null) {
            mServ.resumeMusic();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        //UNBIND music service
        doUnbindService();
        Intent music = new Intent();
        music.setClass(this,MusicService.class);
        stopService(music);

    }

    // GAME
    public void first(){
        over = false;
        Toast.makeText(MainActivity.this, "start", Toast.LENGTH_SHORT).show();
        countDownTimer.start();

        Random rand = new Random();

        for (int i = 0; i < pos.length; i++) {
            int randomIndexToSwap = rand.nextInt(pos.length);
            int temp = pos[randomIndexToSwap];
            pos[randomIndexToSwap] = pos[i];
            pos[i] = temp;
        }

        ImageAdapter imageAdapter = new ImageAdapter(this);
        GridView gridView = (GridView)findViewById(R.id.gridView);
        gridView.setAdapter(imageAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (currentPos < 0 ) {
                    currentPos = position;
                    curView = (ImageView) view;
                    ((ImageView) view).setImageResource(drawable[pos[position]]);
                } else {
                    if (currentPos == position) {
                        ((ImageView) view).setImageResource(R.drawable.hidden);
                    } else if (pos[currentPos] != pos[position]) {
                        curView.setImageResource(R.drawable.hidden);
                        Toast.makeText(MainActivity.this, "Not Match!", Toast.LENGTH_LONG).show();
                    } else {
                        ((ImageView) view).setImageResource(drawable[pos[position]]);
                        countPair++;

                        if (countPair == 8) {
                            countPair = 0;
                            Toast.makeText(MainActivity.this, "You Win!", Toast.LENGTH_LONG).show();

                            if (mInterstitialAd.isLoaded()) {
                                mInterstitialAd.show();
                            } else {
                                Toast.makeText(MainActivity.this, "Interstitial is not loaded.", Toast.LENGTH_LONG).show();
                            }

                            first();
                        }
                    }
                    currentPos = -1;
                }
            }
        });
    }
}

